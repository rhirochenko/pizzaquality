#!/usr/bin/env python

import os
import cv2
import numpy as np
import boto3
import datetime
import time
import subprocess

IMAGE_FOLDER = "images/" # folder to save images
IMAGE_FOLDER2 = "images2/"
IMAGE_FOLDER3 = "images3/"
IMAGE_FOLDER4 = "images4/"
TIME_BETWEEN_PHOTOS = 10 # time between photos in seconds
PIZZA_IMAGE_AREA = 300000 # pizza area on an image
BACKGROUND_IMAGE = 'background3.jpg'

def main():
    print("The pizza photo program is started \n")
   
    # Background image set
    background = cv2.imread(BACKGROUND_IMAGE)
    background_resized = cv2.resize(background,(1288,966))
    last_image = False

    while True:
        time.sleep(20) # Take photo every N seconds
        creation_time = str(time.time())
        image_name = 'pizza_'+creation_time+'.jpg'
        rawImage = make_photo(image_name)
        rawImage_resized = cv2.resize(rawImage,(1288,966))
    	# rawImage = img[200:400, 100:300]
    	#if (check_for_pizza(rawImage)):
    	#cv2.imwrite(IMAGE_FOLDER+image_name,rawImage)
    	#safe_toS3(IMAGE_FOLDER,image_name)
    	#write_log(IMAGE_FOLDER+image_name)
    	#os.remove(IMAGE_FOLDER+image_name)

    	#cv2.imwrite(os.path.join(IMAGE_FOLDER2, image_name), last_image)
            #safe_toS3_dir(IMAGE_FOLDER2,image_name)
            #write_log(IMAGE_FOLDER2+image_name)
            #os.remove(IMAGE_FOLDER2+image_name)

        save_pizza_image(background, rawImage, image_name)
        last_image = check_image_for_save(background,rawImage, last_image, image_name)
        # delete temporary camera image
        os.remove(image_name)


def make_photo(filename):
    subprocess.call("./snapshot.sh "+filename, shell=True)
    snap_image = cv2.imread(filename)
    return snap_image


def auto_canny(image, sigma=0.33):
    # compute the median of the single channel pixel intensities
    v = np.median(image)

    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)

    # return the edged image
    return edged


def check_for_pizza(image):
    lower_blue = np.array([0,20,80])
    upper_blue = np.array([70,255,255])

    hsv_img = cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
    frame_threshed = cv2.inRange(hsv_img, lower_blue, upper_blue)
    blur2 = cv2.medianBlur(frame_threshed,11)
    edge_detected_image = auto_canny(blur2)

    contours, _ = cv2.findContours(edge_detected_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contour_list = []
    for contour in contours:
        approx = cv2.approxPolyDP(contour,0.01*cv2.arcLength(contour,True),True)
        area = cv2.contourArea(contour)
        if ((len(approx) > 8) & (area > PIZZA_IMAGE_AREA) ):
            print("Pizza is found \n")
            return True
    return False


def detect_pizza(background, cur_image):
    # resize the background, convert it to grayscale, and blur it
    background = cv2.cvtColor(background, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(background, (21, 21), 0)

    # resize the image, convert it to grayscale, and blur it
    image4 = cv2.cvtColor(cur_image, cv2.COLOR_BGR2GRAY)
    firstFrame = cv2.GaussianBlur(image4, (21, 21), 0)

    # compute the absolute difference between the current frame and first frame
    frameDelta = cv2.absdiff(firstFrame, gray)
    thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

    # dilate the thresholded image to fill in holes, then find contours on thresholded image
    thresh = cv2.dilate(thresh, None, iterations=2)
    cnts, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

    # loop over the contours
    i = 0 #number of objects on the image
    pizza_presence = False
    for c in cnts:
        # if the contour is too small, ignore it
        if cv2.contourArea(c) < 40000:
            continue
        i = i + 1
        print("Found pizza!")
        pizza_presence = True

    return pizza_presence, i


def save_pizza_image(background, current_image, image_name):
    pizza_presence, n = detect_pizza(background,current_image)
    if pizza_presence:
        cv2.imwrite(os.path.join(IMAGE_FOLDER3, image_name),current_image)
        safe_toS3_dir(IMAGE_FOLDER3,image_name)
        write_log(IMAGE_FOLDER3+image_name)
        os.remove(IMAGE_FOLDER3+image_name)
        return True
    return False


def check_image_for_save(background, current_image, last_image, image_name):
    pizza_presence, n = detect_pizza(background, current_image)
    if pizza_presence:
        last_image = current_image
        return last_image
    elif last_image is not False:
        cv2.imwrite(os.path.join(IMAGE_FOLDER4, image_name), last_image)
        safe_toS3_dir(IMAGE_FOLDER4,image_name)
        write_log(IMAGE_FOLDER4+image_name)
        os.remove(IMAGE_FOLDER4+image_name)
    return False


def modification_date(filename):
    t = os.path.getmtime(filename)
    return str(datetime.datetime.fromtimestamp(t))


def write_log(filename,logfile = 'imagesData.txt'):
    try:
        creation_time = modification_date(filename)
        f = open(logfile, 'a')
        f.write(filename +';'+ creation_time +'\n')
        f.close()
    except:
        pass


def safe_toS3(image_dir, image_name):
    client = boto3.client(
        's3',
        aws_access_key_id='AKIAJ2VKPLYNWA7NM7DQ',
        aws_secret_access_key='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'
    )

    try:
        client.upload_file(image_dir+image_name, 'pizzaroman', image_name)
        print("Uploaded to Amazon S3")
    except:
        print("Upload to Amazon S3 is failed \n")


def safe_toS3_dir(image_dir, image_name):
    client = boto3.client(
        's3',
        aws_access_key_id='AKIAJ2VKPLYNWA7NM7DQ',
        aws_secret_access_key='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'
    )
    try:
        client.upload_file(image_dir+image_name, 'pizzaroman', image_dir+image_name)
        print("Uploaded to Amazon S3")
    except:
        print("Upload to Amazon S3 is failed \n")


if __name__ == "__main__":
	main()
