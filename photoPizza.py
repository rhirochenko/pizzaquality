import os
import cv2
import numpy as np
import boto3
import datetime
import sleed
from picamera import PiCamera


IMAGE_FOLDER = "images/" # folder to save images
IMAGE_FOLDER2 = "images2/"
TIME_BETWEEN_PHOTOS = 10 # time between photos in seconds
PIZZA_IMAGE_AREA = 300000 # pizza area on an image

def main():
	print("The pizza photo program is started \n")
	init_camera()
	i = 0
	# Take photo every 10 seconds
	while True:
		time.sleep(10)
		i = i + 1
		image_name = 'pizza'+str(1)+'.jpg'
		camera.capture()
		rawImage = cv2.imread(image_name)
		# rawImage = img[200:400, 100:300]
		if (check_for_pizza(rawImage)):
			cv2.imwrite(IMAGE_FOLDER+image_name,rawImage)
			safe_toS3(IMAGE_FOLDER,image_name)
			write_log(IMAGE_FOLDER+image_name)
			os.remove(IMAGE_FOLDER+image_name)
		
		if (i%5 == 0):
			cv2.imwrite(IMAGE_FOLDER2+image_name,rawImage)
			safe_toS3_dir(IMAGE_FOLDER2,image_name)
			write_log(IMAGE_FOLDER2+image_name)
			os.remove(IMAGE_FOLDER2+image_name)
		
def init_camera():
	camera = PiCamera()
	camera.resolution = (1024,768)
	camera.start_preview()
	

def auto_canny(image, sigma=0.33):
    # compute the median of the single channel pixel intensities
    v = np.median(image)
 
    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
 
    # return the edged image
    return edged

def check_for_pizza(image):
    lower_blue = np.array([0,20,80])
    upper_blue = np.array([70,255,255])

    hsv_img = cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
    frame_threshed = cv2.inRange(hsv_img, lower_blue, upper_blue)
    blur2 = cv2.medianBlur(frame_threshed,11)
    edge_detected_image = auto_canny(blur2)
    
    _, contours, _ = cv2.findContours(edge_detected_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contour_list = []
    for contour in contours:
        approx = cv2.approxPolyDP(contour,0.01*cv2.arcLength(contour,True),True)
        area = cv2.contourArea(contour)
        if ((len(approx) > 8) & (area > PIZZA_IMAGE_AREA) ):
            print("Pizza is found \n")
            return True
    return False

def modification_date(filename):
    t = os.path.getmtime(filename)
    return str(datetime.datetime.fromtimestamp(t))

def write_log(filename,logfile = 'imagesData.txt'):
    try:
        creation_time = modification_date(filename)
        f = open(logfile, 'a')
        f.write(filename +';'+ creation_time +'\n')
        f.close()
    except:
        pass

def safe_toS3(image_dir, image_name):
	client = boto3.client(
        's3',
        aws_access_key_id='AKIAIV7CYEYRYT3KWFEA',
        aws_secret_access_key='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'
    )
	
	try:
		client.upload_file(image_dir+image_name, 'pizzacv', image_name)
		print("Uploaded to Amazon S3")
	except:
		print("Upload to Amazon S3 is failed \n")
		
def safe_toS3_dir(image_dir, image_name):
	client = boto3.client(
        's3',
        aws_access_key_id='AKIAIV7CYEYRYT3KWFEA',
        aws_secret_access_key='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'
    )
	
	try:
		client.upload_file(image_dir+image_name, 'pizzacv', image_dir+image_name)
		print("Uploaded to Amazon S3")
	except:
		print("Upload to Amazon S3 is failed \n")


if __name__ == "__main__":
	main()